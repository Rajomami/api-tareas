<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tasks', function (Blueprint $table) {
			$table->id();
			$table->string('titulo',255);
			$table->text('descripcion');
			$table->string('estatus_de_complecion',100);
			$table->date('fecha_de_entrega');
			$table->string('comentarios',255)->nullable();
			$table->string('responsable',255)->nullable();
			$table->string('tags',255)->nullable();
			$table->timestamps();
			$table->foreignId('user_id')->constrained();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('tasks');
	}
};

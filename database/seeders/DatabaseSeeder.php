<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{
		$tu1 = 'tokenFijoPostman';
		\App\Models\User::create([
			'name' => 'Raciel Jonathan',
			'email' => 'isc.jonathanmm@gmail.com',
			'password' => bcrypt('password'),
			'token' => $tu1
		]);
		$tu2 = hash('md5', 'tokenDelUsuario2');
		\App\Models\User::create([
			'name' => 'Test User 1',
			'email' => 'test1@example.com',
			'password' => bcrypt('password'),
			'token' => $tu2
		]);
		$tu3 = hash('md5', 'tokenDelUsuario3');
		\App\Models\User::create([
			'name' => 'Test User 2',
			'email' => 'test2@example.com',
			'password' => bcrypt('password'),
			'token' => $tu3
		]);
		$this->command->info("token Usuario1: $tu1");
		$this->command->info("token Usuario2: $tu2");
		$this->command->info("token Usuario3: $tu3");

		\App\Models\Task::create([
			'titulo' => 'Crear el proyecto',
			'descripcion' => 'Primer paso para cumplir los req.',
			'estatus_de_complecion' => '0%',
			'fecha_de_entrega' => '2022-06-27',
			'comentarios' => 'paso 1 de 6',
			'responsable' => 'Jonathan Marquez',
			'tags' => 'desarrollo,test,laravel',
			'user_id' => 1
		]);
		\App\Models\Task::create([
			'titulo' => 'Configurar autenticacion',
			'descripcion' => 'buscar un token en la request para identificar al usuario o denegare el acceso',
			'estatus_de_complecion' => '10%',
			'fecha_de_entrega' => '2022-06-27',
			'comentarios' => 'paso 2 de 6',
			'responsable' => 'Jonathan Marquez',
			'tags' => 'desarrollo,test,laravel',
			'user_id' => 1
		]);
		\App\Models\Task::create([
			'titulo' => 'Crear tablas/columnas necesarias',
			'descripcion' => 'Preparar la base de datos para recibir los datos a utilizar',
			'estatus_de_complecion' => '20%',
			'fecha_de_entrega' => '2022-06-27',
			'comentarios' => 'paso 3 de 6',
			'responsable' => 'Jonathan Marquez',
			'tags' => 'desarrollo,test,laravel',
			'user_id' => 1
		]);
		\App\Models\Task::create([
			'titulo' => 'Crear las rutas',
			'descripcion' => 'Endpoints usados para la api',
			'estatus_de_complecion' => '30%',
			'fecha_de_entrega' => '2022-06-27',
			'comentarios' => 'paso 4 de 6',
			'responsable' => 'Jonathan Marquez',
			'tags' => 'desarrollo,test,laravel',
			'user_id' => 1
		]);
		\App\Models\Task::create([
			'titulo' => 'Dar funcionalidad a las rutas',
			'descripcion' => 'Crear los controladores y validaciones necesarias para el sistema',
			'estatus_de_complecion' => '70%',
			'fecha_de_entrega' => '2022-06-27',
			'comentarios' => 'paso 5 de 6',
			'responsable' => 'Jonathan Marquez',
			'tags' => 'desarrollo,test,laravel',
			'user_id' => 1
		]);
		\App\Models\Task::create([
			'titulo' => 'Hacer las pruebas necesarias',
			'descripcion' => 'Probar la api y crear seeders para su prueba',
			'estatus_de_complecion' => '100%',
			'fecha_de_entrega' => '2022-06-27',
			'comentarios' => 'paso 6 de 6',
			'responsable' => 'Jonathan Marquez',
			'tags' => 'desarrollo,test,laravel',
			'user_id' => 1
		]);
	}
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Task extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array<int, string>
	 */
	protected $fillable = [
		'titulo',
		'descripcion',
		'estatus_de_complecion',
		'fecha_de_entrega',
		'comentarios',
		'responsable',
		'tags',
		'user_id'
	];

	/**
	 * Get the user that owns the Task
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
			return $this->belongsTo(User::class, 'user_id', 'id');
	}

	/**
	 * If $var is not null and exists, look in the title for the value
	 */
	public function scopeFilterByTitulo(Builder $query, String $var = null)
	{
		if(isset($var))
			$query->where('titulo','like',"%$var%");
	}
	/**
	 * If $var is not null and exists, look in the descripcion for the value
	 */
	public function scopeFilterByDescripcion(Builder $query, String $var = null)
	{
		if(isset($var))
			$query->where('descripcion','like',"%$var%");
	}
	/**
	 * If $var is not null and exists, look in the estatus_de_complecion for the value
	 */
	public function scopeFilterByEstatus(Builder $query, String $var = null)
	{
		if(isset($var))
			$query->where('Estatus_de_complecion','like',"%$var%");
	}
	/**
	 * If $var is not null and exists, look in the fecha for the value
	 */
	public function scopeFilterByFecha(Builder $query, String $var = null)
	{
		if(isset($var))
			$query->where('fecha_de_entrega',$var);
	}
}

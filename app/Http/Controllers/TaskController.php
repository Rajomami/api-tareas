<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @param  \App\Http\Requests\StoreTaskRequest  $request
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$perPage = $request->get('perPage', 10);
		$titulo = $request->get('titulo', null);
		$descripcion = $request->get('descripcion', null);
		$estatus = $request->get('estatus', null);
		$fecha = $request->get('fecha', null);

		return Task::filterByTitulo($titulo)
		->filterByDescripcion($descripcion)
		->filterByEstatus($estatus)
		->filterByFecha($fecha)
		->whereUserId(Auth()->user()->id)
		->simplePaginate($perPage);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \App\Http\Requests\StoreTaskRequest  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreTaskRequest $request)
	{
		$user = Auth()->user();
		return $user->tasks()->create($request->only([
			'titulo',
			'descripcion',
			'estatus_de_complecion',
			'fecha_de_entrega',
			'comentarios',
			'responsable',
			'tags'
		]));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Models\task  $task
	 * @return \Illuminate\Http\Response
	 */
	public function show(task $task)
	{
		return $task;
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \App\Http\Requests\UpdateTaskRequest  $request
	 * @param  \App\Models\task  $task
	 * @return \Illuminate\Http\Response
	 */
	public function update(UpdateTaskRequest $request, task $task)
	{
		$task->titulo = $request->titulo;
		$task->descripcion = $request->descripcion;
		$task->estatus_de_complecion = $request->estatus_de_complecion;
		$task->fecha_de_entrega = $request->fecha_de_entrega;
		$task->comentarios = $request->comentarios;
		$task->responsable = $request->responsable;
		$task->tags = $request->tags;
		if($task->save());
		return new JsonResponse(['message'=>'actualizado con exito']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\task  $task
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(task $task)
	{
		if($task->delete())
		return new JsonResponse(['message'=>'Tarea eliminada con exito']);
	}
}

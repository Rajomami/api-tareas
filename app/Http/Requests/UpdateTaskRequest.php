<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTaskRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array<string, mixed>
	 */
	public function rules()
	{
		return [
			'titulo' => 'required',
			'descripcion' => 'required',
			'estatus_de_complecion' => 'required',
			'fecha_de_entrega' => 'required|date_format:Y-m-d',
			'comentarios' => 'nullable',
			'responsable' => 'nullable',
			'tags' => 'nullable'
		];
	}
}

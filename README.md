<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>
 
<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>
 
# Índice
- [Pre-requisitos](#pre-requisitos-)
- [Instalación](#instalación-)
- [Framework y bases](#construido-con-%EF%B8%8F)
- [Licencia](#license)
 
## Pre-requisitos 📋
 
_Herramientas necesarias para poder ejecutar correctamente el sistema_
```
Php >= 8.0.2
Mysql
Servidor web(Apache, Nginx, Lighttpd, etc)
git
```
```
Si no se desea instalar todo lo anterior simplemente use un entorno de desarrollo (laragon, xaamp, etc...)
Asi mismo si utiliza docker habrá notas mas adelante (🐳)
 
```
## Instalación 🔧
 
_Para ejecutar correctamente el sistema se necesita_
 
_Clonar el repositorio sobre el directorio deseado en laragon sería dentro de la carpeta Laragon/../www_
 
_(🐳 Si se usara docker la ubicación puede ser cualquiera)_
 
```
git clone https://gitlab.com/Rajomami/api-tareas.git
```
 
_Colocarte en el directorio api-tareas_
```
cd api-tareas
```
 
_Configurar el proyecto, crear el archivo .env a partir del ejemplo y configurar la base de datos_
```
cp .env.example .env
nano .env
```
_Asegurarse de revisar y configurar las siguientes variables dentro del archivo .env_
```
DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=api_tareas
DB_USERNAME=sail
DB_PASSWORD=password
```
_Ejecutar los siguientes comandos_
```
1-composer install
2-php artisan k:g
```
_(🐳 Si se usara docker instalar las dependencias con el siguiente comando)_
```
docker run --rm \
   -u "$(id -u):$(id -g)" \
   -v $(pwd):/var/www/html \
   -w /var/www/html \
   laravelsail/php81-composer:latest \
   composer install --ignore-platform-reqs
```
## Despliegue 📦
 
_En caso de usar un entorno completo estilo laragon_
```
1-ejecutar laragon, dar click en el botón iniciar
2-revisar los host virtuales o tu archivo .hosts, api-tareas debe quedar registrado automáticamente
3-Revisar tu extensión(por defecto .test)que utilizas para host virtuales
4-En el navegador dirigite a http://api-tareas.test/
```
_(Si no se está usando un entorno como laragon es posible que se deba crear un vhost y agregar un registro al archivo hosts para evitar esos pasos puede utilizar)_
```
php artisan serve
```
_(Si se está usando docker, solo ejecutar)_
```
./vendor/bin/sail up
```
_Esto puede tomar varios minutos dependiendo de su internet y potencia de su equipo_
_Finalmente jecutar las migraciones y seeders_
```
php artisan migrate --seed
```
_(🐳 Si se usara docker se debe ejecutar de la siguiente manera)_
```
./vendor/bin/sail shell
php artisan migrate --seed
```
Al ejecutar de este comando se nos informara sobre los tokens disponibles para poder probar la api, asi mismo se encuentra un archivo para importar una coleccion a postman
## Construido con 🛠️
* [Laravel](https://laravel.com/docs/8.x/installation) - El framework web usado
* [Composer](https://getcomposer.org/) - Manejador de paquetes php
* [mysql](https://www.mysql.com/) - Sistema gestor de base de datos
* [postman](https://www.postman.com/) - Una plataforma para diseñar, contruir y probar API´s
## License
The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
 
